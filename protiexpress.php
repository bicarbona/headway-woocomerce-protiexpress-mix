<?php
/**
 * Plugin Name: Headway Protiexpress
 * Plugin URI:  http://wordpress.org/plugins
 * Description: Just another Headway Themes block!
 * Version:     0.1.0
 * Author:      bicarbona
 * Author URI:  
 * License:     GPLv2+
 * Text Domain: protiexpress
 * Domain Path: /languages
* Bitbucket Plugin URI: https://bicarbona:lucuska@bitbucket.org/bicarbona/headway-woocomerce-protiexpress-mix
* Bitbucket Branch: master
 */


define('PROTIEXPRESS_BLOCK_VERSION', '0.1.0');

/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'protiexpress_register');
function protiexpress_register() {

	if ( !class_exists('Headway') )
		return;

	require_once 'includes/Block.php';
	require_once 'includes/BlockOptions.php';
	//require_once 'design-editor-settings.php';

	return headway_register_block('protiexpressBlock', plugins_url(false, __FILE__));
}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'protiexpress_prevent_404');
function protiexpress_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/**
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'protiexpress_redirect');
function protiexpress_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;
}