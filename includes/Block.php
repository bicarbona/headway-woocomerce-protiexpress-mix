<?php

class protiexpressBlock extends HeadwayBlockAPI {

    public $id = 'protiexpress';
    public $name = 'Protiexpress';
    public $options_class = 'protiexpressBlockOptions';
    public $description = 'Just another Headway Themes block!';

	function init() {
		/* Load dependencies */
	}

	function enqueue_action($block_id) {

 // 	/* JS */
		
	//	wp_enqueue_script('headway-easing-posts', plugin_dir_url(__FILE__) . '/js/script.easing.js', array('jquery'));
		wp_enqueue_script('tabbed-js', plugin_dir_url(__FILE__) . '/js/zozo.tabs.min.js', array('jquery'));

		wp_enqueue_style('tabbed-core', plugin_dir_url(__FILE__) .'/css/zozo.tabs.core.css');		
	//	wp_enqueue_style('tabbed-horizontal', plugin_dir_url(__FILE__) .'/css/zozo.tabs.horizontal.css');
		wp_enqueue_style('tabbed-vertical', plugin_dir_url(__FILE__) .'/css/zozo.tabs.vertical.css');		
		wp_enqueue_style('tabbed-underlined', plugin_dir_url(__FILE__) .'/css/zozo.tabs.underlined.css');		

		// wp_enqueue_style('tabbed-flat-core', plugin_dir_url(__FILE__) .'/css/zozo.tabs.flat.core.css');		
		// wp_enqueue_style('tabbed-flat-styles', plugin_dir_url(__FILE__).'/css/zozo.tabs.flat.styles.css');		
		// wp_enqueue_style('tabbed-flat-themes', plugin_dir_url(__FILE__) .'/css/zozo.tabs.flat.themes.css');		
		// wp_enqueue_style('tabbed-flat-mobile', plugin_dir_url(__FILE__) .'/css/zozo.tabs.flat.mobile.css');		

	}
	
	// public static function init_action($block_id, $block) 
    // {

    // }


    // public static function enqueue_action($block_id, $block, $original_block = null)
    // {

    // }


    // public static function dynamic_css($block_id, $block, $original_block = null)
    // {

    // }


	// function dynamic_js($block_id, $block = false) {
	// 
	// 	if ( !$block )
	// 		$block = HeadwayBlocksData::get_block($block_id);
	// 
	// 	$js = "
	// 	jQuery(document).ready(function() {
	// 		
	// 	});
	// 	";
	// 
	// 	return $js;
	// 
	// }

    // public function setup_elements() {
    //     $this->register_block_element(array(
    //         'id' => 'element1-id',
    //         'name' => 'Element 1 Name',
    //         'selector' => '.my-selector1',
    //         'properties' => array('property1', 'property2', 'property3'),
    //         'states' => array(
    //             'Selected' => '.my-selector1.selected',
    //             'Hover' => '.my-selector1:hover',
    //             'Clicked' => '.my-selector1:active'
    //             )
    //         ));
    // }

//	setlocale(LC_ALL, 'en_US.UTF8');

/**
nazov jedla to ascii --- aby bolo mozne pouzit v (deeplinking)
**/

public	function toAscii($str, $replace=array(), $delimiter='-') {
	 if( !empty($replace) ) {
	  $str = str_replace((array)$replace, ' ', $str);
	 }

	 $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	 $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	 $clean = strtolower(trim($clean, '-'));
	 $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

	 return $clean;
	}

/**
Content
**/

    public function content($block) { 

    					 
						// DOWNLOAD file
							$attachment_id = get_field('mix_doc_download');
							$url = wp_get_attachment_url( $attachment_id );
							$title = get_the_title( $attachment_id );
							 
							if( get_field('mix_doc_download') ):
							    ?><a href="<?php echo $url; ?>" >Download File "<?php echo $title; ?>"</a><?php
							endif;
						 

if(get_field('jedlo'))

    	?>

		<div id="tabbed-nav">
			<?php if(get_field('jedlo')) : ?>
			  <ul class="z-tabs-nav z-tabs-desktop">
			    <?php while(has_sub_field('jedlo')) : $i++; 
				$nazov_jedla = get_sub_field('nazov_jedla');
			    ?>
			    	<li class="z-tab" data-link="<?php echo self::toAscii($nazov_jedla); ?>" ><a class="z-link"><?php the_sub_field('nazov_jedla'); ?></a></li>
			    <?php endwhile; ?>
			  </ul>
			<?php endif;
			 ?>


		<!-- END Tab Navigation Menu -->
		<!-- Content container -->
			<div class="z-container">
			<?php if(get_field('jedlo')) : ?>
				<?php while(has_sub_field('jedlo')) : $i++;  

				$nazov_jedla = get_sub_field('nazov_jedla');
				?>
					<div class="z-content-inner" id="z-content-<?php echo self::toAscii($nazov_jedla); ?>">
						<?php printf('<h2>%s </h2>', $nazov_jedla); ?>


					<?php the_sub_field('nutricna_tabulka'); ?>
					</div>
			<?php endwhile; ?>
			</div>
		</div>
		<?php endif; 

		 ?>
<script>
<?php 
// Prvy nazov
$rows = get_field('jedlo' ); // get all the rows
$first_row = $rows[0]; // get the first row
$first_row_image = $first_row['nazov_jedla' ]; // get the sub field value 
?>
 jQuery(document).ready(function ($) {
     /* jQuery activation and setting options for first tabs*/
   jQuery("#tabbed-nav").zozoTabs({
     	// deactivate: onDeactivate,
		//select: onSelect,
        position: "top-left",
        multiline: true,
		orientation: "vertical",
		responsive: true,
		//        style: "clean",
		style: "underlined",
        theme: "blue",
        spaced: true,
        rounded: false,
        bordered: false,
        dark: false,
		deeplinking: true,
		defaultTab: "<?php echo self::toAscii($first_row_image); ?>",
        animation: {
       //     easing: "easeInOutExpo",
            duration: 450,
            effects: "slideV"
         }
     });
 });
 </script>

<?php
    }
}